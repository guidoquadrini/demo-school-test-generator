class Question < ApplicationRecord
    has_many :questions_exams
    has_many :exams, through: :questions_exams
end
