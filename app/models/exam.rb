class Exam < ApplicationRecord
    has_many :questions_exams
    has_many :questions, through: :questions_exams
end
