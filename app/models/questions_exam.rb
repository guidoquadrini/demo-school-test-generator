class QuestionsExam < ApplicationRecord
    belongs_to :exam
    belongs_to :question
    
    def selectNewQuestionsForTest(exam)
    
        if exam.exam_type == 0
            #2.a.Process Practice Test Regeneration. Query 5 less used questions for Practices
            return selectNewQuestionsForPractice()
        elsif exam.exam_type == 1
            #2.b.Process Exam Test Regeneration. Query 5 unused questions for Exams.
            return selectNewQuestionsForExam()
        end 
    
    end


    private
    def selectNewQuestionsForPractice
        @vRet =  ActiveRecord::Base.connection.execute("
            /*Query the less unused questions for practice-test.*/
            SELECT QF.*, COUNT(QF.id) AS Freq
              FROM (
            SELECT Q.id, Q.title, Q.obs, Q.question_type 
              FROM questions Q
             WHERE Q.question_type = 0 
         UNION ALL
            SELECT Q.id, Q.title, Q.obs, Q.question_type 
              FROM questions_exams QE
        INNER JOIN questions Q ON QE.question_id = Q. id
             WHERE Q.question_type = 0 
                   ) QF
          GROUP BY QF.id
          ORDER BY Freq ASC;
        ")
        return @vRet
    end

    private
    def selectNewQuestionsForExam
        @vRet =  ActiveRecord::Base.connection.execute("
        /*Query unused questions for exam-test.*/
            SELECT Q.id, Q.title, Q.obs, Q.question_type, 1 AS Freq 
              FROM questions Q 
             WHERE Q.id NOT IN(SELECT DISTINCT QE.question_id 
                        FROM questions_exams QE 
                           INNER JOIN questions subQ ON QE.question_id = subQ.id 
                             WHERE subQ.question_type = 1)
               AND Q.question_type = 1;
            ")
        return @vRet
    end

end
