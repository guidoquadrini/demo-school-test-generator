class QuestionsExamsController < ActionController::Base

    def self.regeneratetests
        
        #1.Delete records linked to the exam.
        QuestionsExam.delete_all
        
        #2.Select all exams, for each exam, execute regenerate process made for its kind.
        @exams = Exam.all
        
        @exams.each do |exam| #For each exam in the collection...
           
            modelQuestionsExams = QuestionsExam.new()
            #3.Get Catalog of questions with an extra column "Freq" (Fitness of the Questions).
            questions = modelQuestionsExams.selectNewQuestionsForTest(exam) #Questions have an extra field "Freq" 
            #4.Selection of 5 questions using Roulette Method.
            @selected_questions = selectUsingRouletteMethod(questions, 5)
            #5..Add the questions to current exam.
            @selected_questions.each do |question|     
                exam.questions << Question.find(question['id'])
            end
  
        end 
        
    end
  
    def self.selectUsingRouletteMethod(questions, qtyOfQuestions)
        @catalogo_preguntas = questions  
        @freqAcum = 0
        @selected_questions = Array.new        
        @sumFitness = 0
        @sumatoria = 0
        @catalogo_preguntas.each do |question| @sumFitness += question['Freq'] end
            
        qtyOfQuestions.times do
            @freqAcum = (@catalogo_preguntas[0]['Freq'].to_f / @sumFitness.to_f) * 100
            @random_number = Random.rand(100)  
            $acumRandom.push(@random_number)
            
            @catalogo_preguntas.each do |question| #The questions were previosly orderec by Freq ASC.
                if (@random_number) < (@freqAcum)
                    @selected_questions.push(question)
                    break
                end
                @freqAcum += (question['Freq'].to_f / @sumFitness.to_f) * 100
  
            end
  
        end
        return @selected_questions
        # Roulette Method
        # Explanation: The simple genetic algorithm uses a probabilistic survival rule. In analogy with a problem of the terority of games 
        # (multi-armed bandit problem), John Holland argued that the optimal strategy of selection consists of exponentially increasing 
        # the number of copies of the best observed individual with respect to the but. This method is known as proportional selection. 
        # Ref: Holland, J.H., Adaptation in Natural and Artificial Systems, MIT Press, Second Edition, 1992
        # https://www.researchgate.net/profile/Pablo_Estevez/publication/228708779_Optimizacion_Mediante_Algoritmos_Geneticos/links/0912f51111f82b2a61000000.pdf
    end
  
  

end