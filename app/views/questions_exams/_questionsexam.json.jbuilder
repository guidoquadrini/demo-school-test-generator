json.extract! questionsexam, :id, :exam_id, :question_id, :created_at, :updated_at
json.url questionsexam_url(questionsexam, format: :json)
