class CreateQuestionsExams < ActiveRecord::Migration[5.2]
  def change
    create_table :questions_exams do |t|
      t.belongs_to :exam, index: true, foreign_key: true
      t.belongs_to :question, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
