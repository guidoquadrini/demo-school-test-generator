Rails.application.routes.draw do
  resources :exams
  resources :questions
  get 'welcome/index'
  get 'questions_exams/regeneratetests'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => 'welcome#index'
end
